import Vue from "vue";
import Router from "vue-router";
import Home from "@/views/Home";
import AboutUs from "@/views/AboutUs";
import EventAgency from "@/views/EventAgency";
import StreamingServiceProvider from "@/views/StreamingServiceProvider";
import VirtualPlatformProvider from "@/views/VirtualPlatformProvider";
import VirtualStudioRental from "@/views/VirtualStudioRental";
import IpadRental from "@/views/IpadRental";
import VirtualEvent from "@/views/virtual-event/VirtualAgm";

import ContactUs from "@/views/ContactUs";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home
    },
    {
      path: "/about-us",
      name: "About-us",
      component: AboutUs
    },
    {
      path: "/event-agency",
      name: "Event Agency",
      component: EventAgency
    },
    {
      path: "/streaming-service-provider",
      name: "Streaming Service Provider",
      component: StreamingServiceProvider
    },
    {
      path: "/virtual-platform-provider",
      name: "Virtual Platform Provider",
      component: VirtualPlatformProvider
    },
    {
      path: "/virtual-studio-rental",
      name: "Virtual Studio Rental",
      component: VirtualStudioRental
    },
    {
      path: "/ipad-rental",
      name: "Ipad Rental",
      component: IpadRental
    },
    {
      path: "/virtual-event/virtual-agm",
      name: "Virtual event",
      component: VirtualEvent
    },
    {
      path: "/contact-us",
      name: "Contact-us",
      component: ContactUs
    }
  ]
});
